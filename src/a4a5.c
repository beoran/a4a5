/* Allegro 5 include. */
#include <allegro5/allegro.h>
/* Allegro 4 include. */
#include <allegro.h>

/** Converts an Allegro4 bitmap to an Allegro 5 one. */
ALLEGRO_BITMAP * a4a5_convert_bitmap(void * vbitmap) {
  BITMAP * bitmap = vbitmap;
  return NULL;
}

static ALLEGRO_EVENT_QUEUE * a4a5_keyboard_queue = NULL;
static ALLEGRO_EVENT_QUEUE * a4a5_mouse_queue = NULL;
static ALLEGRO_EVENT_QUEUE * a4a5_joystick_queue = NULL;

#define A4A5_KEYBOARD_BUFFER_SIZE 128

static volatile int a4a5_keyboard_buffer[A4A5_KEYBOARD_BUFFER_SIZE] = { 0 };
static volatile size_t a4a5_keyboard_buffer_size = 0;


void a4a5_clear_keybuf() {
  memset((void *)a4a5_keyboard_buffer, 0 , sizeof(a4a5_keyboard_buffer));
  a4a5_keyboard_buffer_size = 0;
}

int a4a5_keypressed() {
  return (a4a5_keyboard_buffer_size > 0);
}

void a4a5_simulate_keypress(int key) {
  if (a4a5_keyboard_buffer_size < (sizeof(a4a5_keyboard_buffer))) {
    a4a5_keyboard_buffer_size++;
    a4a5_keyboard_buffer[a4a5_keyboard_buffer_size - 1] = key;
  }
}

int a4a5_readkey() {
  int result = 0;
  size_t index;
  while (!a4a5_keypressed()) { ; }
  result = a4a5_keyboard_buffer[0];
  /* shift keystrokes in buffer. */
  for (index = 1; index < (a4a5_keyboard_buffer_size - 1) ; index++) {
    a4a5_keyboard_buffer[index - 1] = a4a5_keyboard_buffer[index];
  }
  a4a5_keyboard_buffer_size--;
  return result;
}

static void a4a5_on_key_down(ALLEGRO_KEYBOARD_EVENT * kev) {
  /* A4 and A5 keycodes are compatible, phew. */
  if (kev->keycode > 0) {
    key[kev->keycode] = TRUE;
  }
}

static void a4a5_on_key_up(ALLEGRO_KEYBOARD_EVENT * kev) {
  /* A4 and A5 keycodes are compatible, phew. */
  if (kev->keycode > 0) {
    key[kev->keycode] = FALSE;
  }
}

static void a4a5_on_key_char(ALLEGRO_KEYBOARD_EVENT * kev) {
  /* A4 and A5 modifiers are compatible, phew. */
  key_shifts = kev->modifiers;
  a4a5_simulate_keypress(kev->keycode);
}


static void * a4a5_keyboard_thread(ALLEGRO_THREAD *thread, void *arg) {
  ALLEGRO_EVENT_QUEUE * queue = arg;
  ALLEGRO_EVENT event;
  while(!al_get_thread_should_stop(thread)) {
    while (al_get_next_event(queue, &event)) {
      switch (event.type) {
         case ALLEGRO_EVENT_KEY_DOWN: a4a5_on_key_down(&event.keyboard); break;
         case ALLEGRO_EVENT_KEY_UP: a4a5_on_key_up(&event.keyboard); break;
         case ALLEGRO_EVENT_KEY_CHAR: a4a5_on_key_char(&event.keyboard); break;
         default: /* Should not happen. */ break;
      }
    }
  }
  al_destroy_event_queue(queue);
  return NULL;
}

/** Sets up a thread to handle Allegro 5 keyboard input and to map this to Allegro 4's state variables. */
ALLEGRO_THREAD * a4a5_start_keyboard_thread(void) {
  ALLEGRO_THREAD * thread = NULL;
  ALLEGRO_EVENT_QUEUE * queue = al_create_event_queue();
  ALLEGRO_EVENT_SOURCE * source = al_get_keyboard_event_source();
  if (!queue) return NULL;
  if (!source) return NULL;
  al_register_event_source(queue, source);
  thread =  al_create_thread(a4a5_keyboard_thread, queue);
  al_start_thread(thread);
  return thread;
}

/** Sets up a thread to handle Allegro 5 mouse input and to map this to Allegro 4's state variables. */
ALLEGRO_THREAD * a4a5_start_mouse_thread(void) {

}

/** Sets up a thread to handle Allegro 5 joystick input and to map this to Allegro 4's state variables. */
ALLEGRO_THREAD * a4a5_start_joystick_thread(void) {

}




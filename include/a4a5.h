#ifndef A4A5_H_INCLUDED
#define A4A5_H_INCLUDED

/* No Allegro 4 include, to prevent pollution. Use void * 
 * and limited includes in stead. */
/* #include <allegro.h> */

#include <allegro/keyboard.h>

/* Allegro 5 include. */
#include <allegro5/allegro.h>

/** Result of a function that could fail. */
typedef enum A4A5_RESULT_ENUM {
  A4A5_RESULT_OK     =  0,
  A4A5_RESULT_ERROR  = -255,
  A4A5_RESULT_ENOMEM = -1,
} A4A5_RESULT ;


ALLEGRO_BITMAP * a4a5_convert_bitmap(void * bitmap);

ALLEGRO_THREAD * a4a5_start_keyboard_thread(void);
ALLEGRO_THREAD * a4a5_start_mouse_thread(void);
ALLEGRO_THREAD * a4a5_start_joystick_thread(void);

/* A4 for use by the client */
extern volatile char key[];

void a4a5_clear_keybuf();
int a4a5_keypressed();
void a4a5_simulate_keypress(int key);
int a4a5_readkey();


#ifndef A4A5_DONT_MAP_A4
#define clear_keybuf a4a5_clear_keybuf
#define keypressed a4a5_keypressed
#define readkey a4a5_readkey
#define simulate_keypress a4a5_simulate_keypress
#endif



#endif

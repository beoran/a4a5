#include "a4a5.h"

#include <stdio.h>
#include <stdlib.h>

#define WIDTH 640
#define HEIGHT 460


void abort_example(char * message) {
  puts(message);
  exit(1);
}


int main(void) {
   ALLEGRO_THREAD * thread;
   ALLEGRO_DISPLAY * display;
   int k = 0;

   if (!(al_install_system(ALLEGRO_VERSION_INT, atexit))) {
      abort_example("Could not init Allegro.\n");
   }

   display = al_create_display(WIDTH, HEIGHT);
   if (!display) {
      abort_example("al_create_display failed\n");
   }
   al_clear_to_color(al_map_rgb_f(0, 0, 0));
   al_flip_display();

   if (!al_install_keyboard()) {
      abort_example("al_install_keyboard failed\n");
   }

   thread = a4a5_start_keyboard_thread();
   if (!thread) {
      abort_example("could not start bridge thread");
   }

   puts("Waiting for Q");
   while (!key[KEY_Q]) {
   }
   
   puts("Clear key buffer.");   
   clear_keybuf();
   
   puts("Waiting for key");
   while (!keypressed()) {
   }
   
   
   while (keypressed()) {
     k = readkey();
     printf("Got key: %d \n", k);
   }


   al_set_thread_should_stop(thread);

   return 0;
}


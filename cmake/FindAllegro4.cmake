find_package(PkgConfig)

# Find main allegro and assume the rest is there too
pkg_check_modules(Allegro4_PKGCONF allegro)

# Perhaps use this later? 
# pkg_search_module(ALLEGRO4 REQUIRED allegro allegro-4)

# MESSAGE("lib: ${Allegro5_PKGCONF_LIBRARY_DIRS}")
# MESSAGE("include: ${Allegro5_PKGCONF_INCLUDE_DIRS}")

# Include dir
find_path(Allegro4_INCLUDE_DIR
  NAMES allegro5/allegro5.h
  PATHS ${Allegro4_PKGCONF_INCLUDE_DIRS}
)

# message("include dir: ${Allegro4_INCLUDE_DIR}")

# Names of all libraries in Allegro, without versions
set(ALLEGRO4_ALL_LIBRARIES
	alleggl alleg jpgalleg
)

# set(ALLEGRO5_LIBRARIES "")

# Find all libraries to link 
foreach(ALLEGRO4_ONE_LIBRARY ${ALLEGRO4_ALL_LIBRARIES})
  # message("${ALLEGRO5_ONE_LIBRARY}")
  find_library("${ALLEGRO4_ONE_LIBRARY}_AID" "${ALLEGRO4_ONE_LIBRARY}"
  ${Allegro4_PKGCONF_LIBRARY_DIRS}
  )  
  set(Allegro4_LIBRARIES
   "${Allegro4_LIBRARIES}" "${${ALLEGRO4_ONE_LIBRARY}_AID}")
  # MESSAGE("${Allegro5_LIBRARIES}")
endforeach(ALLEGRO4_ONE_LIBRARY)
# Remove first empty erroneous "library"
list(REMOVE_AT Allegro4_LIBRARIES 0)
# message("${Allegro4_LIBRARIES}")



# Set the include dir variables and the libraries and let libfind_process do the rest.
set(ALLEGRO4_INCLUDE_DIR "${Allegro4_INCLUDE_DIR}")
set(ALLEGRO4_LIBRARIES "${Allegro4_LIBRARIES}")

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Allegro4 DEFAULT_MSG ALLEGRO4_LIBRARIES ALLEGRO4_INCLUDE_DIR)

MARK_AS_ADVANCED(ALLEGRO4_INCLUDE_DIR ALLEGRO4_LIBRARIES)

